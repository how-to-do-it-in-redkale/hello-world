/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.redkalex.htdi.hello;

import java.io.*;
import java.util.Properties;
import org.redkale.net.http.*;
import org.redkale.service.*;
import org.redkale.util.*;

/**
 *
 * @author zhangjx
 */
@RestService(name = " ")
public class HelloWorldService implements Service {

    private String appVersion = "1.0.0";

    @Override
    public void init(AnyValue conf) {
        String path = "/META-INF/maven/org.redkalex.htdi/htdi-hello/pom.properties";
        InputStream in = this.getClass().getResourceAsStream(path);
        if (in != null) {
            Properties props = new Properties();
            try {
                props.load(in);
                in.close();
            } catch (Exception e) {
            }
            this.appVersion = props.getProperty("version", "未知");
        }
    }

    @RestMapping(name = "index.html", auth = false)
    public HttpResult<String> index(String who) throws IOException {
        if (who == null || who.isEmpty()) who = "World";
        String body = "<!DOCTYPE html>\n"
            + "<html>\n"
            + "<head>\n"
            + "<meta charset=\"UTF-8\">\n"
            + "<title>Hello " + who + "</title>\n"
            + "</head>\n"
            + "<body>\n"
            + "<h1>Hello " + who + "!</h1>\n"
            + "<h3>Project Version: " + appVersion + "</h3>\n"
            + "<h3>Redkale Version: " + Redkale.getDotedVersion() + "</h3>\n"
            + "</body>\n"
            + "</html>";
        return new HttpResult<>("text/html;charset=utf8", body);
    }

}
