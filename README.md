# HelloWorld

#### 项目介绍
How To Do It 系列 redkale 版本的项目，用来介绍 redkale 的常规使用方法的 DEMO 型项目。

#### 章节简介
##### 第一章 HelloWorld

- 使用 MAVEN 管理，集成 redkale 进行演示，可以用于 redkale入门种子项目使用
- 编译:  mvn clean package
- 运行:  bin/start
- 访问:  http://127.0.0.1:6060/index.html